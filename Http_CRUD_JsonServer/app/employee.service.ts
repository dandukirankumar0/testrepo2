import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http:HttpClient) {  // injecting httpclient service in employeeservice


   }


   // url:string ="../assets/data.json";   // real time url java rest api url

    url:string = "http://localhost:3000/employees";


   getById():Observable<Employee>{

      return this.http.get<Employee>(this.url+"/101");


   }


   getAllEmployees():Observable<Employee[]>{

     return this.http.get<Employee[]>(this.url);


   }

   employee = {'id':113,'name':'Deep','salary':450000};



   addEmployee(){

    console.log('add is called..')

   return this.http.post(this.url,this.employee);

   

   }

   updateEmployee(){

    // put()

   }


   deleteEmployee(){

      this.http.delete

   }




}
