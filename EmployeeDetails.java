public class EmployeeDetails {
	int empNo;
	String empName;
	double empSalary;
	public int getEmpNo() {
		return empNo;
	}
	public void setEmpNo(int empNo) {
		this.empNo = empNo;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public double getEmpSalary() {
		return empSalary;
	}
	public void setEmpSalary(double empSalary) {
		this.empSalary = empSalary;
	}
	
}

public class Employee{
	public static void main(String[] args) {
		EmployeeDetails emp1 = new EmployeeDetails();
		
		emp1.setEmpNo(1);
		emp1.setEmpName("kiran");
		emp1.setEmpSalary(33103.75);
        System.out.println("employee id="+ emp1.getEmpNo()+"\nemployee name="+emp1.getEmpName()+"\nemployee salary="+emp1.getEmpSalary());
		
		System.out.println(emp1);
	}
}