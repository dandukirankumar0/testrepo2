import { Component } from '@angular/core';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers:[UserService]
})
export class AppComponent {
  title = "NO Data";



  constructor(private  service:UserService){ // dependency injection



  }

  
    get(){

      this.title =  this.service.getUser();

      

    }



}
