import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'myapp';

  name:string = "JAVEED";

  mydate:Date = new Date();


  user = {'id':101,'name':'tom','amount':8000};

}
