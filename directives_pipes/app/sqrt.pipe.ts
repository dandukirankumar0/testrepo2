import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'sqrt'
})
export class SqrtPipe implements PipeTransform {

  transform(num1:number, ...args: number[]): number {
    return  num1 * args[0];
  }

}
