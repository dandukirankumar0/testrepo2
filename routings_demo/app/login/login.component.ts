import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private route:ActivatedRoute) { }

    username:string = "";
    password:string = "";

  ngOnInit(): void {

       

  }

  getUserData(){

    this.route.params.subscribe(
       (values) => {
          this.username = values['name'];  
          this.password = values['password'];
        })


  }


}
