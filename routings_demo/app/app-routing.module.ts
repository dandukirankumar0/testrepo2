import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactusComponent } from './contactus/contactus.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
    //{path:'',redirectTo:'login',pathMatch:'full'},
    {path:"home",component:HomeComponent},
    {path:"login/:name/:password",component:LoginComponent},
    {path:'contactus',component:ContactusComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
