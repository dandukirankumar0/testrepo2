import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Employee } from './employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private http:HttpClient) {  // injecting httpclient service in employeeservice


   }


    url:string ="../assets/data.json";   // real time url java rest api url

   getAllEmployees():Observable<Employee[]>{

     return this.http.get<Employee[]>(this.url);


   }




}
