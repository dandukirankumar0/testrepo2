import { Component, OnInit } from '@angular/core';
import { Employee } from '../employee';
import { EmployeeService } from '../employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers:[EmployeeService]
})
export class EmployeeComponent implements OnInit {

  constructor(private service:EmployeeService) { }


  empList:Employee[]= [];

  ngOnInit(): void {
  }

  get(){

    this.service.getAllEmployees().
    subscribe( (data) => { console.log(data); this.empList = data });

    console.log(this.empList)
  }



}
