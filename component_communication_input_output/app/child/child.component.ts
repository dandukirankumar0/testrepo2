import { Component, Input, OnInit , EventEmitter, Output} from '@angular/core';


@Component({
  selector: 'app-child',
  templateUrl: './child.component.html',
  styleUrls: ['./child.component.css']
})
export class ChildComponent implements OnInit {

    @Input()  // this meta-data help us to read data from parent to child
    myInput:string = "";  // this var read data from parent



      childData:string = "Hi , I am child sending data";

      //EventEmitter class object for sending data to parent

     @Output() 
     myOutput:EventEmitter<string> =   new EventEmitter();

     sendData(){

        this.myOutput.emit(this.childData);


     }


  constructor() { 

 
  }

  ngOnInit(): void {


    console.log(this.myInput); // here we are printing data coming from parent 


  }

}
