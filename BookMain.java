
public class Book
{
   int isbn;
   float price,discount;
   String author,title;
   
   
   Book(int isbn,float price,String author,String title)
   {
       this.isbn=isbn;
       this.price=price;
       this.author=author;
       this.title=title;
   }
   
   void display()
   {
       System.out.println("ISBN no is:"+isbn+"\nPrice of book is:"+price+"\nAuthor is:"+author+"\nTitle of the book is:"+title);
   }
   
   void discount(int disc)
   {
       
       this.discount=price*disc/100;
       this.price=price-discount;
       
       System.out.println("The price of the product after discount is:"+price);
       
   }
}
public class BookMain
{
    public static void main(String args[])
    {
        Book b= new Book(1029,399,"james","Java");
        b.display();
        b.discount(18);
        
    }
}